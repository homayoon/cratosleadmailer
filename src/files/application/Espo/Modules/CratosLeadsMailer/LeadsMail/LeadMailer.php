<?php


namespace Espo\Modules\CratosLeadsMailer\LeadsMail;

use Espo\Core\Mail\EmailSender;
use Espo\Core\Mail\EmailFactory;

class LeadMailer
{
    private $emailSender;

    private $emailFactory;

    public function __construct(EmailSender $emailSender, EmailFactory $emailFactory)
    {
        $this->emailSender = $emailSender;
        $this->emailFactory = $emailFactory;
    }

    public function send($subject, $title, $body, $isHtml = true): void
    {
        $email = $this->emailFactory->create();

        $email->setSubject($title);
        $email->setBody($body);
        $email->addToAddress($subject);
        if(!$isHtml)
            $email->setIsPlain();

        $this->emailSender->send($email);
    }
}
