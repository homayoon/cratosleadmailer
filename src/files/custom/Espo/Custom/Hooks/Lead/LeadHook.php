<?php

namespace Espo\Custom\Hooks\Lead;

use Espo\Modules\CratosLeadsMailer\LeadsMail\LeadMailer;
use Espo\ORM\Entity;

class LeadHook {

    private $mailer;

    public function __construct(LeadMailer $leadMailer)
    {
        $this->mailer = $leadMailer;
    }

    public function afterSave(Entity $entity, array $options) :void
    {
        $to = 'info@trustworld.co';
        $title = "سلام دنیا";
        $body = "<h1>new Lead Entity Created</h1><br/><h3>Details: </h3>";
        $arr = json_decode(json_encode($entity->getValueMap()), true);
        foreach ($arr as $key => $value){
            $value = $value ? $value : '-';
            $body .= "<p><strong>$key: </strong>$value</p>";
        }

        $this->mailer->send($to,$title,$body);
    }
}
